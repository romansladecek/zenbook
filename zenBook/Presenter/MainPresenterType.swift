//
//  MainPresenterType.swift
//  zenBook
//
//  Created by Roman Sladecek on 11/11/2019.
//  Copyright © 2019 Roman Sladecek. All rights reserved.
//

import UIKit

protocol MainPresenterType {
    func requestData(for orientation: UIDeviceOrientation, disableRemoteReload: Bool?)
}

// protocol with default value
extension MainPresenterType {
    func requestData(for orientation: UIDeviceOrientation) {
        return requestData(for: orientation, disableRemoteReload: false)
    }
}
