//
//  MainPresenter.swift
//  zenBook
//
//  Created by Roman Sladecek on 10/11/2019.
//  Copyright © 2019 Roman Sladecek. All rights reserved.
//

import UIKit

final class MainPresenter: MainPresenterType {
    public var delegate: MainViewControllerType?
    var parser = BookParser()
    var books = [Book]()

    /// Requests for new data (server or just locally if device orientation
    /// has been changed.
    ///
    /// - Parameters:
    ///     - orientation: Device orientation
    ///     - disableRemoteReload: Do not load new data from server
    func requestData(for orientation: UIDeviceOrientation, disableRemoteReload: Bool? = false) {
        if disableRemoteReload == true {
            delegate?.displayData(with: makeViewModel(from: books, for: orientation))
        } else {
            parser.parse { books in
                guard let books = books else {
                    print("Parsing failed")
                    return
                }
                self.books = books
                delegate?.displayData(with: makeViewModel(from: self.books, for: orientation))
            }
        }
    }

    /// Makes viewModel from parsed books and device orientation
    ///
    /// - Parameters:
    ///     - books: Parsed book objects from XML
    ///     - orientation: Device orientation
    ///
    /// - Returns: viewModel for tableView
    private func makeViewModel(from books: [Book], for orientation: UIDeviceOrientation) -> BooksTableViewModel {
        var tableViewModel = BooksTableViewModel()
        var cellViewModel = BooksCellViewModel()
        let numberOfItemsInRow = Constants.Settings.itemsPerRow(mode: orientation.isPortrait ? .portrait : .landscape).count
        books.forEach { book in
            let bookViewModel = BookViewModel(
                title: book.title,
                thumbnailUrl: book.thumbnailUrl,
                isNew: book.isNew
            )
            cellViewModel.append(bookViewModel)
            // if row is full-filled with items
            if cellViewModel.count == numberOfItemsInRow {
                tableViewModel.append(cellViewModel)
                 // clean for new row
                cellViewModel.removeAll()
            }
        }
        // if some books left in las non-full-filled cell, put it in separate cell at the end
        if !cellViewModel.isEmpty {
            tableViewModel.append(cellViewModel)
        }
        return tableViewModel
    }
}
