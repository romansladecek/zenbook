//
//  AppDelegate.swift
//  zenBook
//
//  Created by Roman Sladecek on 10/11/2019.
//  Copyright © 2019 Roman Sladecek. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }
}

