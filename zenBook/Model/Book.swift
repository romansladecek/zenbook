//
//  Book.swift
//  zenBook
//
//  Created by Roman Sladecek on 10/11/2019.
//  Copyright © 2019 Roman Sladecek. All rights reserved.
//

import Foundation

struct Book {
    var title: String
    var thumbnailUrl: String
    var isNew: Bool
}
