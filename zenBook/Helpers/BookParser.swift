//
//  BookParser.swift
//  zenBook
//
//  Created by Roman Sladecek on 11/11/2019.
//  Copyright © 2019 Roman Sladecek. All rights reserved.
//
///
/// My custom XML parser suited for our Bookshelf XML
///

import Foundation

final class BookParser: NSObject {
    private let elementNameTypeBook = "BOOK"
    private let elementNameTypeTitle = "TITLE"
    private let elementNameTypeThumbnail = "THUMBNAIL"
    private let elementNameTypeIsNew = "NEW"
    private let elementNameValueIsNewTrue = "TRUE"

    private var elementName: String = String()
    private var books: [Book] = []
    private var bookTitle = String()
    private var bookThumbnailUrl = String()
    private var bookIsNew = false

    private lazy var parser: XMLParser? = {
        guard let urlPath = URL(string: Constants.Server.baseUrl) else {
            print("Failed to get urlPath for XML")
            return nil
        }
        return XMLParser(contentsOf: urlPath)
    }()

    override init() {
        super.init()
        parser?.delegate = self
    }

    public func parse(completion: (_ books: [Book]?) -> Void) {
        guard parser?.parse() == true else {
            completion(nil)
            return
        }
        completion(books)
    }
}

extension BookParser: XMLParserDelegate {
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        if elementName == elementNameTypeBook {
            bookTitle = ""
            bookThumbnailUrl = ""
            bookIsNew = false
        }
        self.elementName = elementName
    }

    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == elementNameTypeBook {
            let book = Book(
                title: bookTitle,
                thumbnailUrl: bookThumbnailUrl,
                isNew: bookIsNew
            )
            books.append(book)
        }
    }

    func parser(_ parser: XMLParser, foundCharacters string: String) {
        let data = string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        guard !data.isEmpty else { return }
        if elementName == elementNameTypeTitle {
            bookTitle += data
        } else if elementName == elementNameTypeThumbnail {
            bookThumbnailUrl += data
        } else if elementName == elementNameTypeIsNew {
            bookIsNew = data == elementNameValueIsNewTrue
        }
    }
}
