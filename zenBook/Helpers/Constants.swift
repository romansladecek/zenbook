//
//  Constants.swift
//  zenBook
//
//  Created by Roman Sladecek on 11/11/2019.
//  Copyright © 2019 Roman Sladecek. All rights reserved.
//
///
/// Important Constants and setup of app
///

import Foundation

struct Constants {
    struct Server {
        static let baseUrl = "URL"
    }
    enum Settings {
        case itemsPerRow(mode: ScreenOrientation)

        // number of items in row
        var count: Int {
            switch self {
            case .itemsPerRow(mode: .portrait):
                return 2 // tested maximum for 6 items per row (iPhone 11)
            case .itemsPerRow(mode: .landscape):
                return 3 // tested maximum for 9 items per row (iPhone 11)
            }
        }

        // experimentally choosen item heights for each orientation
        var rowHeight: Int {
            switch self {
            case .itemsPerRow(mode: .portrait):
                return (10 - count) * 25
            case .itemsPerRow(mode: .landscape):
                return (14 - count) * 25
            }
        }
    }
}
