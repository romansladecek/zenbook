//
//  UIView+Subviews.swift
//  zenBook
//
//  Created by Roman Sladecek on 11/11/2019.
//  Copyright © 2019 Roman Sladecek. All rights reserved.
//

import UIKit

extension UIView {
    // Removes all subviews of view
    func removeAllSubviews() {
        subviews.forEach { view in
            view.removeFromSuperview()
        }
    }

    // Pin view to its superview
    func fixInView(_ container: UIView!) {
        translatesAutoresizingMaskIntoConstraints = false
        frame = container.frame
        container.addSubview(self)
        NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
    }
}
