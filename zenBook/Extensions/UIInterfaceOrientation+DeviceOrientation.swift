//
//  UIInterfaceOrientation+DeviceOrientation.swift
//  zenBook
//
//  Created by Roman Sladecek on 11/11/2019.
//  Copyright © 2019 Roman Sladecek. All rights reserved.
//

import UIKit

extension UIInterfaceOrientation {
    // recognition of device orientation for app init state purposes
    var deviceOrientation: UIDeviceOrientation {
        let orientation = UIApplication.shared.statusBarOrientation
        if orientation == .landscapeLeft {
            return .landscapeLeft
        } else if orientation == .landscapeRight {
            return .landscapeRight
        } else {
            return .portrait // or unknown
        }
    }
}

public enum ScreenOrientation {
    case portrait, landscape
}
