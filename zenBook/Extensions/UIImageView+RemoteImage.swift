//
//  UIImageView+RemoteImage.swift
//  zenBook
//
//  Created by Roman Sladecek on 11/11/2019.
//  Copyright © 2019 Roman Sladecek. All rights reserved.
//

import UIKit

extension UIImageView {
    // Downloads image from URL (async)
    func downloadImage(from url: URL?, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        guard let url = url else { return }
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
        }.resume()
    }
    // Download image from String url (async)
    func downloadImage(from link: String?, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        guard let link = link, let url = URL(string: link) else { return }
        downloadImage(from: url, contentMode: mode)
    }
}
