//
//  ViewController.swift
//  zenBook
//
//  Created by Roman Sladecek on 10/11/2019.
//  Copyright © 2019 Roman Sladecek. All rights reserved.
//

import UIKit

protocol MainViewControllerType {
    func displayData(with viewModel: BooksTableViewModel)
}

final class MainViewController: UITableViewController {
    lazy var presenter: MainPresenterType = {
        let presenter = MainPresenter()
        presenter.delegate = self
        return presenter
    }()

    private var viewModel = BooksTableViewModel()

    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        presenter.requestData(for: UIApplication.shared.statusBarOrientation.deviceOrientation)
    }

    // device orientation changed
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        presenter.requestData(for: UIDevice.current.orientation, disableRemoteReload: true)
    }

    private func setupViews() {
        view.backgroundColor = .brown
        tableView.tableFooterView = UIView()
        tableView.separatorInset = .zero
        tableView.separatorColor = .clear
    }

    // MARK: UITableViewDataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "booksRowCell", for: indexPath) as? BooksRowTableViewCell else {
            return UITableViewCell()
        }
        cell.configureCell(with: viewModel[indexPath.row])
        return cell
    }
}

extension MainViewController: MainViewControllerType {
    /// Reload table with new data
    ///
    /// - Parameters:
    ///     - viewModel: presenter viewModel for tableView
    ///
    func displayData(with viewModel: BooksTableViewModel) {
        self.viewModel = viewModel
        tableView.reloadData()
    }
}
