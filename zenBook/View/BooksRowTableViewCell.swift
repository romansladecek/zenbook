//
//  BooksRowTableViewCell.swift
//  zenBook
//
//  Created by Roman Sladecek on 10/11/2019.
//  Copyright © 2019 Roman Sladecek. All rights reserved.
//

import UIKit

typealias BooksCellViewModel = [BookViewModel]
typealias BooksTableViewModel = [BooksCellViewModel]

struct BookViewModel {
    let title: String
    let thumbnailUrl: String
    let isNew: Bool
}

final class BooksRowTableViewCell: UITableViewCell {
    @IBOutlet private weak var stackView: UIStackView!

    func configureCell(with viewModel: BooksCellViewModel) {
        isUserInteractionEnabled = false
        backgroundColor = .brown
        
        stackView.removeAllSubviews()
        stackView.distribution = .fillEqually
        stackView.alignment = .leading
        stackView.spacing = 16.0

        viewModel.forEach { bookViewModel in
            let bookView = BookView()
            // number of books in row is nonlinear to it's height
            let itemHeight = Constants.Settings.itemsPerRow(
                mode: UIApplication.shared.statusBarOrientation.isPortrait ? .portrait : .landscape
                ).rowHeight
            bookView.heightAnchor.constraint(equalToConstant: CGFloat(itemHeight)).isActive = true
            bookView.configureView(with: bookViewModel)
            stackView.addArrangedSubview(bookView)
        }
    }
}
