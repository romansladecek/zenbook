//
//  BookView.swift
//  zenBook
//
//  Created by Roman Sladecek on 11/11/2019.
//  Copyright © 2019 Roman Sladecek. All rights reserved.
//

import UIKit

protocol BookViewType {
    func configureView(with viewModel: BookViewModel)
}

final class BookView: UIView, BookViewType {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var isNewImageView: UIImageView!
    @IBOutlet weak var bookCoverImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }

    private func setupView() {
        Bundle.main.loadNibNamed("BookView", owner: self, options: nil)
        contentView.fixInView(self)
    }

    public func configureView(with viewModel: BookViewModel) {
        titleLabel.text = viewModel.title
        bookCoverImageView.downloadImage(from: viewModel.thumbnailUrl)
        bookCoverImageView.isHidden = !viewModel.isNew
    }
}
